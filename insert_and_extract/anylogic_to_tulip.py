from tulip import *
from anygraph import extract_railways
<<<<<<< HEAD
=======
import shapely.geometry as geo


def graph_to_tulip( graph ):
    """
    Convert the given NetworkX graph into
    a Tulip Graph
    """




>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b

def export_graph_to_tulip( graph, export_path ):
    """
    Expecting a graph of switch -> containing a
    geometry of geojson.  Creates a tulip file
    """

    from tulip import tlp

    g = tlp.newGraph()

    # Properties for the resulting graph
    id = g.getStringProperty("id")
    name = g.getStringProperty("name")
    geometry = g.getStringProperty("geometry")

    node_by_id = {}

    # Export each node
    for node_id, attr in graph.nodes_iter(data=True):
        log.debug("Tulip - node {}".format(node_id))
        # A new tulip node
        node = g.addNode()
        node_by_id[node_id] = node
        id[node] = attr['id']
        name[node] = attr['name']
        geometry[node] = str(attr['geometry'])
        # Set some special Tulip properties to place the nodes on the canvas
        g['viewLabel'][node] = attr['name']
        g['viewLayout'][node] = tlp.Coord(attr['geometry'].x, attr['geometry'].y)

    # Export each edge
    for u_id, v_id, attr in graph.edges_iter(data=True):
        log.debug("Tulip - vertex {}_{}".format(u_id,v_id))
        # Find the source and target Tulip nodes
        u = node_by_id[u_id]
        v = node_by_id[v_id]
        # A new tulip edge from these
        edge = g.addEdge(u, v)
        # Copy across attributes into Tulip
        id[node] = attr['id']
        name[node] = attr['name']
        geometry[node] = str(attr['geometry'])
        # Set some special Tulip properties to curve edges to look like tracks
        g['viewLabel'][edge] = attr['name']
        # Omit the first and last coordinates for tulip edges
        edge_coordinates = [tlp.Coord(*xy) for xy in attr['geometry'].coords[1:-1]]
        g['viewLayout'][edge] = edge_coordinates

    log.info("ExportTulip to {}".format(export_path))

    tlp.saveGraph( g, export_path )
