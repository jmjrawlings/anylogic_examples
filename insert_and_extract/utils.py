__author__ = "Justin Rawlings"

"""
Utility functions for dealing with
AnyLogic XML schema as present in
their project file (.alp)
"""

import xml.etree.ElementTree as etree

# Constant XML field references
TRACK = "RailTrack"
SWITCH = "RailroadSwitch"
RAILYARD = "Railyard"
SHAPES = "Shapes"
X = "X"
Y = "Y"
POINT = "Point"
OFFSET = "Offset"
NAME = "Name"
PRESENT = "PresentationFlag"
SOURCEID = "SourceId"
TARGETID = "TargetId"
ID = "Id"


def get_many(element, **kwargs):
    """
    Get and cast many fields from the provided
    XML element
    """
    return {key: get(element, key, cast) for key, cast in kwargs.items()}


def get(element, key, type=str):
    """
    Get a value from the given XML element under
    a key and cast it
    """
    value = element.find(key).text
    return type(value)


def get_xy( element ):
    # Get the (x,y) coordinates of the element
    return get( element, X, float), get(element, Y, float)


def parse_alp( file ):
    return etree.parse(file)