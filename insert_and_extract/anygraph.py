__author__ = 'Justin Rawlings'

"""
anygraph.py

Provides convenience methods to take a given AnyLogic Project
FIle (.alp) and extract some of the networks within as
NetworkX graphs.


"""

import xml.etree.ElementTree as et
<<<<<<< HEAD
import copy
import networkx as nx
import logging
import shapely.geometry as geo
import shapely.affinity as affine

=======
import networkx as nx
import logging
from utils import *
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger("anygraph")

<<<<<<< HEAD
# Constant XML field references
TRACK = "RailTrack"
SWITCH = "RailroadSwitch"
RAILYARD = "Railyard"
SHAPES = "Shapes"
X = "X"
Y = "Y"
POINT = "Point"
OFFSET = "Offset"
NAME = "Name"
PRESENT = "PresentationFlag"
SOURCEID = "SourceId"
TARGETID = "TargetId"
ID = "Id"


def get_many(element, **kwargs):
    """
    Get and cast many fields from the provided
    XML element
    """
    return {key: get(element, key, cast) for key, cast in kwargs.items()}


def get(element, key, type=str):
    """
    Get a value from the given XML element under
    a key and cast it
    """
    value = element.find(key).text
    return type(value)

def get_xy( element ):
    # Get the (x,y) coordinates of the element
    return get( element, X, float), get(element, Y, float)
=======
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b

def get_geometry_for_track(track):
    """
    Returns a list of (x,y) tuples representing
    the relative coordinates of the given
    RailwayTrack XML

    AnyLogic keeps this information in the Points XML.
    Confusingly the Points XML contains a mixture of
    'actual' vertices, and 'guiding line' handles.

    The guiding handles are circles the user drags around
    to adjust the curvature of the Track (Bezier curves?)

    Specifically, there seems to be 3 'Point' elements for each
    single actual Point on the Track.

     - 'actual'
     - start handle
     - end handle

    To this end we extract only every third one

    """

    # The sequence of points for this track
    points = track.getiterator(tag=POINT)

    coords = []
    # Only process every third point (as per bezier curves)
    for point in points[::3]:
        x = get(point, X, float)
        y = -get(point, Y, float)
        coords.append((x, y))

    # Form a geometric linestring from the coordinates
    line = geo.LineString(coords)

    log.debug("\t{}vertices, {}km".format(len(line.coords), line.length))

    return line

<<<<<<< HEAD
=======
def add_switches( yard, graph ):
    """
    Add railway switches from the Yard to the Graph
    """
    switches = yard.getiterator(tag=SWITCH)
    n = len(switches)

    log.debug("- {} switches found".format( n ))

    for index, switch in enumerate(switches):

        name =get( switch, NAME )

        log.debug("\t{} - {}".format(index, name))

        # Add as a node to the graph
        graph.add_node( name, xml=switch )
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b


def add_tracks( yard, graph ):
    """
    Creates a graph edge for each track in the yard

    Tracks that do not have either a Source or Target switch will
    have one created so we can store them in this format.

    Tracks with neither a source or target switch will be removed
    """

    tracks = yard.getiterator(tag=TRACK)
    n = len(tracks)

    log.info("- {} tracks found".format( n ))

<<<<<<< HEAD
    # Since a track doesn't need two switches, we will load edges of the graph first
    for index, track in enumerate(tracks):
        # Interrogate XML for info
        id = get( track, ID )
=======
    # We need an ID -> Name lookup for the Switches
    id_to_switch = {}
    for switch, data in graph.nodes(data=True):
        switch_id = get( data['xml'], ID )
        id_to_switch[switch_id] = switch

    def get_switch( id ):
        return id_to_switch.get(id)

    # Since a track doesn't need two switches, we will load edges of the graph first
    for index, track in enumerate(tracks):
        # Interrogate XML for info
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b
        name = get( track, NAME )
        log.debug("\t{}/{} Track {}".format(index+1, n, name))

        # Tracks store a source and target switch ID if they are connected
<<<<<<< HEAD
        source = track.attrib.get(SOURCEID)
        target = track.attrib.get(TARGETID)

        # A track with no source or target is isolated - we will ignore those
        if not (source or target):
=======
        source_id = track.attrib.get(SOURCEID)
        target_id = track.attrib.get(TARGETID)
        # Map to switches
        source = get_switch( source_id )
        target = get_switch( target_id )

        # A track with no source or target is isolated - we will ignore those
        if not (source_id or target_id):
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b
            log.warn("\tignoring track {}".format(name))
            continue

        # Create Source node if required
<<<<<<< HEAD
        if not source:
            log.info("new source")
            # Use track name as id
            source = "{}_source".format( id )
            # Add this non-switch node
            graph.add_node( source, xml=None )

        elif not target:
            log.info("new target")
            # Use track name as id
            target = "{}_target".format( id )
=======
        if not source_id:
            log.info("new source")
            # Use track name as id
            source = "{}_source".format( name )
            # Add this non-switch node
            graph.add_node( source, xml=None )

        elif not target_id:
            log.info("new target")
            # Use track name as id
            target = "{}_target".format( name )
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b
            # Add this non-switch node
            graph.add_node( target, xml=None )

        log.debug("\t{} -> {}".format(source, target))

        # Add this as an edge to the graph
<<<<<<< HEAD
        graph.add_edge(source, target, id=id, xml=track)

def add_switches( yard, graph ):
    """
    Add railway switches from the Yard to the Graph
    """
    switches = yard.getiterator(tag=SWITCH)
    n = len(switches)

    log.debug("- {} switches found".format( n ))

    for index, switch in enumerate(switches):

        id = get( switch, ID )
        name =get( switch, NAME )

        log.debug("\t{} - {}".format(index, name))

        # Add as a node to the graph
        graph.add_node( id, xml=switch)

=======
        graph.add_edge(source, target, xml=track)
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b


def convert_yard_to_graph( yard ):
    """
    Convert an AnyLogic XML representation of a
    RailYard to a directed multigraph.

    RailwaySwitch become nodes
    RailwayTrack become edges

    """

    # Some basic info
    name = get( yard, NAME )
    x,y = get_xy( yard )

    log.info("Converting Yard {}".format( name ))

    # Instantiate the graph
    graph = nx.MultiDiGraph(name=name, x=x, y=y)

    # Create from XML elements
    add_switches( yard, graph )
    add_tracks( yard, graph )

    return graph


def extract_railways( alp_file ):
    """
    Parse an Anylogic .alp file, extracting
    any RailwayNetworks found therein as
    a Networx DirectedMultiGraph

    Return as an iterable
    """

    log.info("Parsing {}".format(alp_file))

    # Parse the ".alp" (an XML)
<<<<<<< HEAD
    model = et.parse(alp_file)
=======
    model = parse_alp(alp_file)
>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b

    # Seek out RailYards
    yards = model.getiterator(tag=RAILYARD)
    n = len(yards)

    log.info("{} Railyards found".format(n))

    # Convert each yard to a graph
    for index, yard in enumerate(yards):
        log.debug("Yard {}/{}".format(index+1, n))

        yield convert_yard_to_graph(yard)


<<<<<<< HEAD
def create_points_on_track( track, offset_a=10.0, offset_b=100.0):

    #Find the shapes for this track
    track_shapes = track.find("Shapes")
    for shape in track_shapes.getchildren():
        track_shapes.remove(shape)

    # What is our track name?
    track_name = track.find("Name").text.split("_")[-1]
    point_name = track_name.replace("track","position")

    # First Point on track at the start
    point_a = copy.deepcopy(self.templates["point"])
    point_a.find("Offset").text = str(offset_a)
    point_a.find("Name").text = point_name + "_start"
    point_a.find("PresentationFlag").text="false"

    # Second point on track at the end
    point_b = copy.deepcopy(point_a)
    point_b.find("Offset").text = str(offset_b)
    point_b.find("Name").text = point_name + "_end"
    point_b.find("PresentationFlag").text="false"

    # Add to this track (it now has 2 points on track)
    track_shapes.append(point_a)
    track_shapes.append(point_b)

def create_vertex( parent, x,y):

    point = et.SubElement(parent,"Point")

    sub_x = et.SubElement(point, "X")
    sub_y = et.SubElement(point, "Y")
    sub_z = et.SubElement(point, "Z")

    sub_x.text = str(x)
    sub_y.text = str(y)
    sub_z.text = '0.0'

    return point

if __name__ == '__main__':
    alp = "railway.alp"
    graphs = extract_railways(alp)
    for graph in graphs:
        print graph
=======
if __name__ == '__main__':
    alp = "networks.alp"
    graphs = extract_railways(alp)
    for graph in graphs:

        print '{} has {} nodes and {} edges'.format(
            graph.name,
            graph.number_of_nodes(),
            graph.number_of_edges()
        )

        for switch in graph.nodes():
            # How many tracks end here
            out_degree = graph.out_degree( switch )
            # How many tracks start here
            in_degree = graph.in_degree( switch )
            # Total
            degree = graph.degree( switch )
            # These should match
            assert degree == (in_degree + out_degree )
            # Switches should be connected to max 3 tracks
            assert 1 <= degree <= 3

            print "{} ( {} in + {} out = {} total )".format(
                switch, in_degree, out_degree, degree
            )



>>>>>>> eb0e0e3540c56373c2fa36ec343a45fecd43291b




